package data

import language.implicitConversions
import spire.math.UByte
import spire.math.UInt
import spire.math.ULong
import spire.math.UShort

import scala.annotation.tailrec

/**
 * Contains a number of commonly used unsigned value types and provides corresponding implicit conversions.
 */
package object unsigned {

  private[this] val TwoPower63: BigInt = BigInt(1) << 63

  // Performance
  private[this] val BigInt0: BigInt = implicitly[Numeric[BigInt]].zero

  private[unsigned] def bigIntFromULong(l: Long): BigInt = {

    // Convert the positive part
    val a = BigInt(l & 0x7fffffffffffffffL)

    // Convert the MSB separately
    val b = if ((l & 0x8000000000000000L) != 0) TwoPower63 else BigInt0

    // Combine in the BigInt domain
    a | b
  }

  // Implicit conversions between unsigned types
  implicit def uByte2UShort(x: UByte): UShort = UShort(x.toShort)
  implicit def uByte2UInt(x: UByte): UInt = UInt(x.toInt)
  implicit def uByte2ULong(x: UByte): ULong = ULong(x.toLong)
  implicit def uShort2UInt(x: UShort): UInt = UInt(x.toInt)
  implicit def uShort2ULong(x: UShort): ULong = ULong(x.toLong)
  implicit def uInt2uLong(x: UInt): ULong = ULong(x.toLong)

  // Implicit classes for converting signed types to the unsigned
  implicit class FloatOps(val x: Float) extends AnyVal {
    def toUByte: UByte = UByte(x.toByte)
    def toUShort: UShort = UShort(x.toShort)
    def toUInt: UInt = UInt(x.toInt)
    def toULong: ULong = ULong(x.toLong)
  }

  implicit class DoubleOps(val x: Double) extends AnyVal {
    def toUByte: UByte = UByte(x.toByte)
    def toUShort: UShort = UShort(x.toShort)
    def toUInt: UInt = UInt(x.toInt)
    def toULong: ULong = ULong(x.toLong)
  }

  implicit class SignedByteOps(val x: Byte) extends AnyVal {
    def toUByte: UByte = UByte(x)
    def toUShort: UShort = UShort(x.toShort)
    def toUInt: UInt = UInt(x.toInt)
    def toULong: ULong = ULong(x.toLong)
  }

  implicit class SignedShortOps(val x: Short) extends AnyVal {
    def toUByte: UByte = UByte((x & 0xff).toByte)
    def toUShort: UShort = UShort(x)
    def toUInt: UInt = UInt(x.toInt)
    def toULong: ULong = ULong(x.toLong)
  }

  implicit class SignedIntOps(val x: Int) extends AnyVal {
    def toUByte: UByte = UByte((x & 0xff).toByte)
    def toUShort: UShort = UShort((x & 0xffff).toShort)
    def toUInt: UInt = UInt(x)
    def toULong: ULong = ULong(x.toLong)
  }

  implicit class SignedLongOps(val x: Long) extends AnyVal {
    def toUByte: UByte = UByte((x & 0xffL).toByte)
    def toUShort: UShort = UShort((x & 0xffffL).toShort)
    def toUInt: UInt = UInt((x & 0xffffffffL).toInt)
    def toULong: ULong = ULong(x)
  }

  implicit class BigIntOps(val x: BigInt) extends AnyVal {
    def toUByte: UByte = UByte((x & 0xffL).toByte)
    def toUShort: UShort = UShort((x & 0xffffL).toShort)
    def toUInt: UInt = UInt((x & 0xffffffffL).toInt)
    def toULong: ULong = ULong(x.toLong)
  }

  /**
   * A `StringLike` like string enrichment providing convenience functions to convert a string to an unsigned integer.
   *
   * The implementation throws the same exceptions as the plagiated scala functions
   * that convert strings to numeric values.
   */
  implicit class UnsignedStringEnrichment(private val underlying: String) extends AnyVal {
    def toUByte: UByte = underlying.toShort.toUByte
    def toUShort: UShort = underlying.toInt.toUShort
    def toUInt: UInt = underlying.toLong.toUInt
    def toULong: ULong = BigInt(underlying).toULong
  }

  /**
   * Generates a regular expression allowing only numeric values of 0 - [max value] inclusive.
   *
   * The max value has to be passed as sequence of single digit integers.
   */
  private[unsigned] def pattern(digitsOfMaxValue: Seq[Int]): String = {
    assert(
      digitsOfMaxValue.forall(int => 0 <= int && int <= 9),
      s"Only single digit integers allowed as argument, but got $digitsOfMaxValue"
    )
    val maxWidth = digitsOfMaxValue.size

    assert(
      maxWidth >= 3,
      s"This function only generates regular expression patterns for values at least as big as UByte -> 255. " +
        s"Got ${digitsOfMaxValue.mkString}"
    )

    @tailrec
    def nexTier(digits: Seq[Int], prefix: String, acc: String): String = {
      digits match {
        case head +: Seq() =>
          acc + "|" + prefix + (if (head == 0) "0" else s"[0-$head]")
        case 0 +: tail =>
          nexTier(tail, prefix + "0", acc)
        case head +: tail =>
          nexTier(tail, prefix + head, acc + s"|$prefix[0-${head - 1}]" + """\d{""" + tail.size + "}")

      }
    }

    val staticHeader = {
      "(?:" +
        // until we get to the full width of the max value everything is allowed ... simple
        """0*\d{1,""" + s"${maxWidth - 1}}"
    }
    // closing the non-capturing group of regex statements concatenated with '|'
    val staticEnd = ")"

    // NOTE: the very first digit CANNOT be '0'
    (digitsOfMaxValue match {
      case 1 +: tail =>
        nexTier(tail, "1", staticHeader)
      case 2 +: tail =>
        nexTier(tail, "2", staticHeader + s"|1" + """\d{""" + tail.size + "}")
      case head +: tail =>
        nexTier(tail, s"$head", staticHeader + s"|[1-${head - 1}]" + """\d{""" + tail.size + "}")
    }) + staticEnd
  }
}
