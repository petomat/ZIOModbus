package data.unsigned

/**
 * Supplies some tools to extract specific bytes from integers and longs as well as build such values from bytes.
 */
object UnsignedOperations {
  /**
   * Converts the passed `Byte` value to a `Short` value.
   */
  final def asUnsignedInt8(b: Byte): Short = (b & 0xff).toShort

  /**
   * Converts the passed high and low `Byte` values to an `Int` value.
   */
  final def asUnsignedInt16(high: Byte, low: Byte): Int = (high & 0xff) << 8 | (low & 0xff)

  /**
   * Converts the passed `Byte` values to a `Long` value.
   *
   * @param hh Higher high byte or byte number 4.
   * @param hl Lower high  byte or byte number 3.
   * @param lh Higher high byte or byte number 2.
   * @param ll Lower low byte or byte number 1.
   */
  final def asUnsignedInt32(hh: Byte, hl: Byte, lh: Byte, ll: Byte): Long = {
    ((hh & 0xffL) << 24) | ((hl & 0xffL) << 16) | ((lh & 0xffL) << 8) | (ll & 0xffL)
  }

  /**
   * Returns the left most byte of the passed `Int`.
   */
  final def lowByteOfInt16(i: Int): Byte = (i & 0xff).toByte

  /**
   * Returns the right most byte of the passed `Int`.
   */
  final def highByteOfInt16(i: Int): Byte = ((i >> 8) & 0xff).toByte

  /**
   * Returns the byte at the passed position of the passed `Long` value
   *
   * or `00` if position is outside the range of Long.
   *
   * @param n The desired bytes position.
   * @param i The `Long` value to extract the byte from.
   */
  final def byteOfInt32(n: Int, i: Long): Byte = {
    if (n >= 1 && n <= 4) ((i >> (8 * (n - 1))) & 0xffL).toByte else 0x00.toByte
  }

  /**
   * Returns the byte at the passed position of the passed `Int` value.
   *
   * or `00` if position is outside the range of Int.
   *
   * @param n The desired bytes position.
   * @param i The `Int` value to extract the byte from.
   */
  final def byteOfInt16(n: Int, i: Int): Byte = {
    if (n >= 1 && n <= 2) ((i >> (8 * (n - 1))) & 0xff).toByte else 0x00.toByte
  }

  /**
   * Builds an `Int` value from four consecutive bytes within the passed byte array starting at the passed offset.
   *
   * @param bytes The byte array to extract the bytes from.
   * @param offset The offset position within the byte array from where to start.
   */
  final def asInt(bytes: Array[Byte], offset: Int): Int = {
    (((bytes(offset + 0) & 0xff) << 24)
    | ((bytes(offset + 1) & 0xff) << 16)
    | ((bytes(offset + 2) & 0xff) << 8)
    | ((bytes(offset + 3) & 0xff) << 0))
  }

}
