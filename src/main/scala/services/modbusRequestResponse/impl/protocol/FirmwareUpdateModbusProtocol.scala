package services.modbusRequestResponse.impl.protocol

import services.modbusRequestResponse.api.protocol.FunctionCode
import services.modbusRequestResponse.api.protocol.ModbusProtocol
import services.modbusRequestResponse.api.protocol.ModbusRequest
import services.modbusRequestResponse.api.protocol.ModbusResponse
import zio.Chunk
import zio.Trace

// FIXME: just dummy here, to implement

case object FirmwareUpdateModbusProtocol extends ModbusProtocol {
  sealed trait Request extends ModbusRequest[Response]
  object Request {
    case object Dummy extends Request {
      type Response = Response.Dummy
      val functionCode: FunctionCode = ???
      def requestData: Chunk[Byte] = Chunk.empty
      def createResponseFromBytes(bytes: Chunk[Byte])(implicit trace: Trace): Option[Response] = Some(Response.Dummy)
      def responseBodyLength: Int = 0
      def possibleExceptionCodes: Set[ModbusResponse.ExceptionCode] = Set.empty
    }
  }
  sealed trait Response extends ModbusResponse
  object Response {
    sealed trait Dummy extends Response
    case object Dummy extends Dummy
  }
}
