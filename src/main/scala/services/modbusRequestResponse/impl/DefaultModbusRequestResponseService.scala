package services.modbusRequestResponse.impl

import zio._
import services.modbusRequestResponse.api.ModbusRequestResponseService
import services.modbusRequestResponse.api.SlaveId
import services.modbusRequestResponse.api.protocol.ErrorCode
import services.modbusRequestResponse.api.protocol.FunctionCode
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.minSize
import services.modbusRequestResponse.api.protocol.ModbusRequest
import services.modbusRequestResponse.api.protocol.ModbusResponse
import services.serialPort.api.SerialPortService
import utils.SynchronizedOps


final class DefaultModbusRequestResponseService[Req <: ModbusRequest[Resp], Resp <: ModbusResponse](
  // Ref used to guard a write-request and read-response pair to be isolated (i.e. without interleaving)
  // E.g.: Write1, Write2, Read1, Read2 may happen. We want: Write1, Read1, Write2, Read2
  // FIXME write test case
  serialPortServiceRef: Ref.Synchronized[SerialPortService],
) extends ModbusRequestResponseService[Req, Resp] {

  private type ZIOModbus[+R] = ZIO[Any, ModbusRequestResponseService.Error, R]

  def request(slaveId: SlaveId, request: Req): ZIOModbus[request.Response] = {
    // https://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf
    val responseEnvelopeLength: Int = ByteLengths.slaveId + request.responseLength + ByteLengths.crcLength
    val RequestEnvelope: ZIOModbus[Chunk[Byte]] = {
      for {
        _ <- ZIO
          .fail(ModbusRequestResponseService.Error.PayloadExceeded)
          .when(responseEnvelopeLength > ByteLengths.maxSerialBuffer)
        requestEnvelope <- ZIO.succeed(CRC16.appendCRC(slaveId.toBytes ++ request.payload))
        _ <- ZIO
          .fail(ModbusRequestResponseService.Error.PayloadExceeded)
          .when(requestEnvelope.length > ByteLengths.maxSerialBuffer)
      } yield requestEnvelope
    }
    def sendRequest(serialPortService: SerialPortService, bytes: Chunk[Byte]): ZIOModbus[Unit] = {
      serialPortService.writeBytes(bytes).orElseFail(ModbusRequestResponseService.Error.Writing)
    }
    def readResponseBytes(serialPortService: SerialPortService): ZIOModbus[Chunk[Byte]] = {
      for {
        responseBytesWithCRC <- serialPortService.readBytesAtMost(responseEnvelopeLength)
          .orElseFail(ModbusRequestResponseService.Error.Reading)
        _ <- ZIO.fail(ModbusRequestResponseService.Error.ResponseTooShort).when(responseBytesWithCRC.length < minSize)
      } yield responseBytesWithCRC
    }
    def checkCRC(responseBytesWithCRC: Chunk[Byte]): ZIOModbus[Chunk[Byte]] = {
      ZIO.fromOption(CRC16.check(responseBytesWithCRC)).orElseFail(ModbusRequestResponseService.Error.CRC)
    }
    def checkSlaveId(responseBytesWithoutCRC: Chunk[Byte]): ZIOModbus[Chunk[Byte]] = {
      def e: ModbusRequestResponseService.Error = {
        ModbusRequestResponseService.Error.SlaveMismatch(slaveId, responseBytesWithoutCRC.take(ByteLengths.slaveId))
      }
      ZIO.fromOption(SlaveId.FromBytes.unapply(responseBytesWithoutCRC))
        .collect(e) { case (`slaveId`, tail) => tail }
        .orElseFail(e)
    }
    def deserializeResponse(responseBytesWithoutSlaveIdCRC: Chunk[Byte]): ZIOModbus[request.Response] = {
      responseBytesWithoutSlaveIdCRC match {
        case FunctionCode.FromHeadOfBytes(request.functionCode, bytes) =>
          ZIO.fromOption(request.createResponseFromBytes(bytes))
            .orElseFail(ModbusRequestResponseService.Error.ResponseParsing(request.functionCode, bytes))
        case ErrorCode.FromHeadOfBytes(request.errorCode, bytes) =>
          bytes match {
            case ModbusResponse.ExceptionCode.FromBytes(exceptionCode)
              if request.possibleExceptionCodes.contains(exceptionCode) =>
              ZIO.fail(
                ModbusRequestResponseService.Error.Modbus(
                  ModbusResponse.ExceptionResponse(request.functionCode, exceptionCode)
                )
              )
            case _ =>
              ZIO.fail(ModbusRequestResponseService.Error.ExceptionParsing(request.functionCode, bytes))
          }
        case other =>
          ZIO.fail(
            ModbusRequestResponseService.Error.FunctionErrorCodeMismatch(
              request.functionCode,
              other.take(ByteLengths.functionOrErrorCode)
            )
          )
      }
    }
    def writeReqReadResp(requestEnvelope: Chunk[Byte]): ZIOModbus[Chunk[Byte]] = {
      serialPortServiceRef.foreachZIO { serialPortService =>
        sendRequest(serialPortService, requestEnvelope) *> readResponseBytes(serialPortService)
      }
    }
    RequestEnvelope flatMap writeReqReadResp flatMap checkCRC flatMap checkSlaveId flatMap { deserializeResponse(_) }
  }
}

object DefaultModbusRequestResponseService {
  def live[
    Req <: ModbusRequest[Resp]: Tag, Resp <: ModbusResponse: Tag
  ]: ZLayer[SerialPortService, Nothing, ModbusRequestResponseService[Req, Resp]] = {
    ZLayer {
      for {
        sps <- ZIO.service[SerialPortService]
        spsRef <- Ref.Synchronized.make(sps)
      } yield {
        new DefaultModbusRequestResponseService[Req, Resp](spsRef)
      }
    }
  }
}
