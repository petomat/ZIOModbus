package services.modbusRequestResponse.api

import data.unsigned.SignedIntOps
import spire.math.UShort
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants

// FIXME yeah, number of versus count vs length (vs amount)
case class NumberOfRegisters private (underlying: Int) extends AnyVal {
  def shortValue: Short = underlying.toShort
}

object NumberOfRegisters {
  def apply(number: UShort): NumberOfRegisters = {
    require(
      number <= ModbusMessageConstants.maxPayloadRegisters.toUShort,
      s"NumberOfRegisters must be less than maximum number of register that fit into the buffer."
    )
    new NumberOfRegisters(number.toInt)
  }
}
