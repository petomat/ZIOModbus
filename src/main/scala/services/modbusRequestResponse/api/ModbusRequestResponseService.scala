package services.modbusRequestResponse.api

import services.modbusRequestResponse.api.protocol.ModbusRequest
import services.modbusRequestResponse.api.protocol.ModbusResponse
import zio.Chunk
import zio.Tag
import zio.ZIO

trait ModbusRequestResponseService[Req <: ModbusRequest[Resp], Resp <: ModbusResponse] {
  // Envelope: Who = slaveId, What = request
  def request(slaveId: SlaveId, request: Req): ZIO[Any, ModbusRequestResponseService.Error, request.Response]
}

object ModbusRequestResponseService {
  // same for all protocols?:
  sealed trait Error extends Product with Serializable
  object Error {

    /** The bytes could not be written successfully. */
    case object Writing extends Error

    /** Bytes could not be read successfully. */
    case object Reading extends Error

    /** Response too underlying, can not contain the envelope. */
    case object ResponseTooShort extends Error

    /** The slave id of the response does not match the slave of the request. */
    case class SlaveMismatch(expected: SlaveId, actual: Chunk[Byte]) extends Error

    /** The expected response could not be parsed to a valid response. */
    case class ResponseParsing(functionCode: protocol.FunctionCode, responseBody: Chunk[Byte]) extends Error

    /** The expected exception could not be parsed to a valid exception. */
    case class ExceptionParsing(functionCode: protocol.FunctionCode, responseBody: Chunk[Byte]) extends Error

    /** The returned function / error code does not match the request. */
    case class FunctionErrorCodeMismatch(functionCode: protocol.FunctionCode, code: Chunk[Byte]) extends Error

    /** The request returned with a valid exception response. */
    case class Modbus(error: ModbusResponse.ExceptionResponse) extends Error

    /** The CRC checksum did not match. */
    // This trait makes it possible to use trait name as an error type as opposed to the case object named followed by ".type"
    sealed trait CRC extends Error
    case object CRC extends CRC

    /** The max payload for the request was exceeded. */
    // This trait makes it possible to use trait name as an error type as opposed to the case object named followed by ".type"
    sealed trait PayloadExceeded extends Error
    case object PayloadExceeded extends PayloadExceeded
  }
  // Accessors:
  def request[Req <: ModbusRequest[Resp]: Tag, Resp <: ModbusResponse: Tag](
    slaveId: SlaveId,
    request: Req
  ): ZIO[ModbusRequestResponseService[Req, Resp], ModbusRequestResponseService.Error, request.Response] = {
    ZIO.serviceWithZIO[ModbusRequestResponseService[Req, Resp]](_.request(slaveId, request))
  }
}
