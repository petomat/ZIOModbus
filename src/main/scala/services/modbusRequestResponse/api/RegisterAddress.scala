package services.modbusRequestResponse.api

import spire.math.UShort

/**
 * Representation of a modbus register address,
 *
 * As specified in 'Modbus Application Protocol 1.1 b3'.
 *
 * @param underlying Underlying `primitive` value able to represent the documented address range.
 *                   'In a MODBUS PDU each data is addressed from 0 to 65535.'
 */
case class RegisterAddress private (underlying: Int) extends AnyVal {
  def shortValue: Short = underlying.toShort
}

object RegisterAddress {
  def apply(underlying: UShort): RegisterAddress = {
    new RegisterAddress(underlying.toInt)
  }
}
