package services.modbusRequestResponse.api

import data.unsigned.SignedByteOps
import spire.math.UByte
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths
import zio.Chunk

case class SlaveId private (byte: Short) extends AnyVal {
  def toBytes: Chunk[Byte] = Chunk(byte.toByte)
}

object SlaveId {
  def apply(byte: UByte): SlaveId = {
    new SlaveId(byte.shortValue)
  }

  object FromBytes {
    def unapply(bytes: Chunk[Byte]): Option[(SlaveId, Chunk[Byte])] = {
      bytes.splitAt(ByteLengths.slaveId) match {
        case (Chunk(byte), tail) if byte > 0 =>
          // not the implicit conversion from byte to short would give us negative shorts
          Some(SlaveId(byte.toUByte) -> tail)
        case _ => None
      }
    }
  }
}
