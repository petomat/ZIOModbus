package services.modbusRequestResponse.api.protocol

import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths
import zio.Chunk

case class FunctionCode private (byte: Byte) extends AnyVal {

  /** Returns the corresponding error code. */
  def errorCode: ErrorCode = {
    // In an exception response the used function code is exactly 80 hexadecimal higher than the value would be for a
    // normal response. This method returns the corresponding error code for this function code.
    // Bytes are signed in Java, so this will overflow to the negative range, but it will work fine.
    ErrorCode((byte + 0x80).toByte)
  }

  def toBytes: Chunk[Byte] = Chunk(byte)
}

object FunctionCode {

  // the latter is technically not possible because all numeric types are signed in Scala.
  // FIXME intellij says the right hand side is always true, is it?
  def isValid(byte: Byte): Boolean = byte > 0 && byte <= 0x80

  def apply(byte: Byte): FunctionCode = {
    require(isValid(byte), s"Function code must be between zero (excl.) and 0x80 (incl.), but was $byte")
    new FunctionCode(byte)
  }

  object FromHeadOfBytes {
    def unapply(bytes: Chunk[Byte]): Option[(FunctionCode, Chunk[Byte])] =
      bytes.splitAt(ByteLengths.functionOrErrorCode) match {
        case (Chunk(byte), tail) if isValid(byte) => Some(FunctionCode(byte) -> tail)
        case _ => None
      }
  }
}
