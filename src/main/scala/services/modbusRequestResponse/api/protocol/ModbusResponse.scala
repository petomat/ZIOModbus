package services.modbusRequestResponse.api.protocol

import zio.Chunk

trait ModbusResponse

object ModbusResponse {
  // The function code here is not the shifted one but the normal one. In an exception response the used function code
  // is exactly 80 hexadecimal higher than the value would be for a normal response.
  case class ExceptionResponse(functionCode: FunctionCode, exceptionCode: ExceptionCode) {
    override def toString: String = s"ExceptionResponse(functionCode=$functionCode, exceptionCode=$exceptionCode)"
  }

  sealed trait ExceptionCode extends Product with Serializable

  // Same for all modbus protocols?:
  object ExceptionCode {

    object FromBytes {
      def unapply(bytes: Chunk[Byte]): Option[ExceptionCode] = bytes match {
        case Chunk(byte) => fromByte(byte)
        case _ => None
      }
    }

    // this is not baked into the cases since we maybe want to split the cases to an API and the byte stuff to the implementation
    def fromByte(byte: Byte): Option[ExceptionCode] = {
      // See https://modbus.org/docs/Modbus_Application_Protocol_V1_1b.pdf page 49
      byte match {
        case 1 => Some(IllegalFunction)
        case 2 => Some(IllegalDataAddress)
        case 3 => Some(IllegalDataValue)
        case 4 => Some(SlaveDeviceFailure)
        case 5 => Some(Acknowledge)
        case 6 => Some(SlaveDeviceBusy)
        // 7 is not specified
        case 8 => Some(MemoryParityError)
        // 9 is not specified
        case 10 => Some(GatewayPathUnavailable)
        case 11 => Some(GatewayTargetDeviceFailedToRespond)
        case _ => None
      }
    }
    // FIXME add scaladoc according to modbus spec
    case object IllegalFunction extends ExceptionCode
    case object IllegalDataAddress extends ExceptionCode
    case object IllegalDataValue extends ExceptionCode
    case object SlaveDeviceFailure extends ExceptionCode
    case object Acknowledge extends ExceptionCode
    case object SlaveDeviceBusy extends ExceptionCode
    case object MemoryParityError extends ExceptionCode
    case object GatewayPathUnavailable extends ExceptionCode
    case object GatewayTargetDeviceFailedToRespond extends ExceptionCode
  }
}
