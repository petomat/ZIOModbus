package services.modbusRequestResponse.api.protocol

object ModbusMessageConstants {
  // Serialized bytes between a modbus master and slave are constructed like:
  // Part name:        SlaveId, FunctionOrErrorCode, RequestData, CRC
  // Part byte length:    1   ,          1         ,  <=252     ,  2
  // So, we can transmit up to 252/2=126 registers
  //
  // FunctionOrErrorCode: First half of byte until 0x80 = 128 are for function codes;
  //     second half of byte are for corresponding error codes for function codes: error code = function code + 0x80.
  object ByteLengths {
    val slaveId: Int = 1
    val functionOrErrorCode: Int = 1
    val crcLength: Int = 2
    // FIXME konfigurierbar machen, der SPI chip kann gerade nur 128 statt 256
    val maxSerialBuffer: Int = 128
    val maxPayload: Int = maxSerialBuffer - slaveId - functionOrErrorCode - crcLength
    val register: Int = 2
    val byteCount: Int = 1
    val responseEnvelope: Int = ByteLengths.functionOrErrorCode + ByteLengths.crcLength
  }
  val maxPayloadRegisters: Int = {
    ByteLengths.maxPayload / ByteLengths.register - math.ceil(ByteLengths.responseEnvelope / ByteLengths.register).toInt
  }
  val minSize: Int = ByteLengths.slaveId + ByteLengths.functionOrErrorCode + ByteLengths.crcLength
}
