package services.modbusRequestResponse.api.protocol

import zio.Chunk
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths


case class ErrorCode private (byte: Byte) extends AnyVal

object ErrorCode {

  // the latter is technically not possible because all numeric types are signed in Scala.
  // FIXME intellij says the right hand side is always false, should we use unsigned byte data type?
  def isValid(byte: Byte): Boolean = byte <= 0 || byte > 0x80

  def apply(byte: Byte): ErrorCode = {
    require(isValid(byte), s"Error code must not be between 0 (excl.) and 0x80 (incl.), but was $byte")
    new ErrorCode(byte)
  }

  object FromHeadOfBytes {
    def unapply(bytes: Chunk[Byte]): Option[(ErrorCode, Chunk[Byte])] =
      bytes.splitAt(ByteLengths.functionOrErrorCode) match {
        case (Chunk(byte), tail) if isValid(byte) => Some(ErrorCode(byte) -> tail)
        case _ => None
      }
  }
}
