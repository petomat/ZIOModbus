package services.serialPort.impl

import java.io.IOException
import purejavacomm.CommPortIdentifier
import purejavacomm.NoSuchPortException
import purejavacomm.PortInUseException
import purejavacomm.PureJavaIllegalStateException
import purejavacomm.SerialPort
import purejavacomm.UnsupportedCommOperationException
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths
import services.serialPort.api.SerialPortService
import utils._
import zio.Chunk
import zio.Duration
import zio.Ref
import zio.Scope
import zio.ZIO
import zio.ZLayer
import zio.durationInt


// FIXME: error type!
final class PureJavaCommSerialPortService(
  val readTimeout: Duration,
  val serialPortRef: Ref.Synchronized[SerialPort]
) extends SerialPortService {
  import PureJavaCommSerialPortService.mapExceptions

  def readBytesAtMost(count: Int): ZIO[Any, String, Chunk[Byte]] = {
    for {
      // FIXME: reconnect on failure?
      arr <- ZIO.succeed(new Array[Byte](count))
      read <- serialPortRef.foreachZIO { serialPort =>
        ZIO.attempt {
          serialPort.enableReceiveThreshold(count)
          serialPort.getInputStream.read(arr, 0, count)
        }.mapError(mapExceptions)
      }
    } yield Chunk.fromArray(arr).take(read)
  }

  def writeBytes(bytes: Chunk[Byte]): ZIO[Any, String, Unit] = {
    serialPortRef.foreachZIO { serialPort =>
      ZIO.attempt {
        serialPort.getOutputStream.write(bytes.toArray)
      }.mapError(mapExceptions)
    }
  }
}

object PureJavaCommSerialPortService {
  // Note: ZIO currently inconsistently names the "unhappy" path a Failure
  // (see https://zio.dev/reference/error-management/types/) but also Error in methods like zio.ZIO#mapError.
  // These failure types are coming from prue java comm but seem to be general.
  // Moreover these types are only for implementation factories.
  sealed trait Failure extends Product with Serializable

  object Failure {
    case object NoSuchPort extends Failure
    case object PortInUse extends Failure
    case class IllegalConfiguration(reason: String) extends Failure
    // FIXME some kind of fallback type for other reasons. which of them are implementation specific?
    case class Other(reason: String) extends Failure
  }

  private val mapExceptions: PartialFunction[Throwable, String] = {
    case e: PureJavaIllegalStateException => e.toString
    case io: IOException => io.toString
  }

  private[this] val acquire: String => ZIO[Any, Failure, PureJavaCommSerialPortService] = { port =>
    // FIXME config:
    val readTimeoutMillis = 50 // FIXME whats a good value?
    val openTimeoutMillis = 1000
    val baudRate = 115200
    val dataBits = 0
    val stopBits = 2
    val parityBits = 0
    val appname = port
    // FIXME: ZIO.attemptBlockingInterrupt or alike
    def mapUnsupportedCommOp: PartialFunction[Throwable, Failure.IllegalConfiguration] = {
      case e: UnsupportedCommOperationException => Failure.IllegalConfiguration(e.getMessage)
    }
    ZIO.blocking {
      for {
        serialPortId <- ZIO.attempt(CommPortIdentifier.getPortIdentifier(port))
          .refineOrDie[Failure] { case _: NoSuchPortException => Failure.NoSuchPort }
        serialPort <- ZIO.attempt(serialPortId.open(appname, openTimeoutMillis))
          .refineOrDie[Failure] { case _: PortInUseException => Failure.PortInUse }
          .filterTypeOrElse[SerialPort](ZIO.fail(Failure.Other(s"commPort $serialPortId isn't a serial port")))
        _ <- ZIO.attempt(serialPort.setSerialPortParams(baudRate, dataBits, stopBits, parityBits))
          .refineOrDie[Failure](mapUnsupportedCommOp)
        _ <- ZIO.attempt(serialPort.enableReceiveTimeout(readTimeoutMillis))
          .refineOrDie[Failure](mapUnsupportedCommOp)
        // FIXME Do we need to flush stale data?
        serialPortRef <- Ref.Synchronized.make(serialPort)
      } yield {
        new PureJavaCommSerialPortService(readTimeoutMillis.millis, serialPortRef)
      }
    }
  }

  private val release: PureJavaCommSerialPortService => ZIO[Any, Nothing, Unit] = { service =>
    service.serialPortRef.foreach(_.close())
  }

  def scoped(port: String): ZIO[Scope, Failure, SerialPortService] = {
    ZIO.acquireRelease(
      ZIO.debugToConsoleBeforeAfter(s"Opening port $port...")(acquire(port))(_ => s"Opened $port")
    ) { s =>
      ZIO.debugToConsoleBeforeAfter(s"Closing port $port...")(release(s))(_ => s"Closed $port")
    }
  }

  // FIXME: port via config?
  def live(port: String): ZLayer[Any, Failure, SerialPortService] = ZLayer.scoped(scoped(port))
}
