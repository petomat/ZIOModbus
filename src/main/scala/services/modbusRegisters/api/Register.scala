package services.modbusRegisters.api

import services.modbusRequestResponse.api.protocol.ModbusMessageConstants
import spire.math.UByte
import zio.Chunk

/**
 * Representation of a modbus register.
 *
 * This is used for both
 *  - Input Register
 *  - Holding Register
 *
 * 'MODBUS logical reference numbers, which are used in MODBUS functions,
 * are unsigned integer indices starting at zero.'
 *
 * @param bytes The raw data representation at last used in serial communication.
 */
case class Register private (bytes: Chunk[Byte]) extends AnyVal

object Register {

  /**
   * Enforces the usage of UByte for Register creation.
   *
   * @param upper The upper byte of the 16-bit register word.
   * @param lower The lower byte of the 16-bit register word.
   */
  def apply(upper: UByte, lower: UByte): Register = Register(Chunk(upper.toByte, lower.toByte))

  /**
   * Creates a `Register` instance from incoming bytes of a serial stream.
   */
  def fromBytes(chunk: Chunk[Byte]): Register = {
    require(
      chunk.length == ModbusMessageConstants.ByteLengths.register,
      s"Unexpected register bytes, expected ${ModbusMessageConstants.ByteLengths.register} bytes," +
        s" but got ${chunk.length}"
    )
    Register(chunk)
  }
}
