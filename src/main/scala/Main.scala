import data.unsigned.SignedIntOps
import services.modbusRegisters.api.ModbusRegistersService
import services.modbusRegisters.impl.DefaultModbusRegistersService
import services.modbusRequestResponse.api.NumberOfRegisters
import services.modbusRequestResponse.api.RegisterAddress
import services.modbusRequestResponse.api.SlaveId
import services.modbusRequestResponse.impl.DefaultModbusRequestResponseService
import services.modbusRequestResponse.impl.protocol.RWModbusProtocol
import services.serialPort.impl.PureJavaCommSerialPortService
import utils._
import zio.Console
import zio.Scope
import zio.ZIO
import zio.ZIOAppArgs
import zio.ZIOAppDefault

// killall -9 java ; time /data/zulu17.40.19-ca-jdk17.0.6-linux_aarch32hf/bin/java -jar ZIOModbus-assembly-0.1.0-SNAPSHOT.jar

// FIXME search for all these orDie
object Main extends ZIOAppDefault {
  // FIXME: Error types
  // On SVt:
  // Times are heavily dependent on how often we repeat reading (probably JIT compiler)
  // Read 4 registers took 23 ms
  // Read 27 registers took 33 ms
  // Read 60 registers took 42 ms (repeated 100 times)
  // Read 60 registers took 22 ms (repeated 1000 times)

  private val repetitions = 3000

  private[this] val app: ZIO[ModbusRegistersService, String, Unit] = {
    code {
      for {
        _ <- ZIO.foreachPar(1 to repetitions) { _ =>
          ModbusRegistersService.readRegisters(
            SlaveId(4.toUByte),
            registerOffset = RegisterAddress(0),
            NumberOfRegisters(9)
          )
        }.withParallelism(10)
      } yield ()
    }.debugTimedToConsole(s"$repetitions repetitions")
  }
  val run: ZIO[ZIOAppArgs with Scope, Any, Any] = {
    val providedApp = {
      app.provide(
        DefaultModbusRegistersService.live,
        DefaultModbusRequestResponseService.live[RWModbusProtocol.Request, RWModbusProtocol.Response],
        // JSerialCommSerialPortService.live(port = "/dev/ttySNS1")
        PureJavaCommSerialPortService.live(port = "/dev/ttySNS1")
      )
    }
    (Console.printLine("START") *> providedApp <* Console.printLine("END")).withTimedConsole
  }
}
