package services.modbusRequestResponse

import services.modbusRequestResponse.impl.CRC16
import zio.Chunk
import zio.Scope
import zio.ZIO
import zio.test.Gen
import zio.test.Spec
import zio.test.TestAspect
import zio.test.TestEnvironment
import zio.test.ZIOSpecDefault
import zio.test.assertTrue
import zio.test.check

object CRC16Spec extends ZIOSpecDefault {

  // FIXME test case that shows: for all b: Chunk[Bytes] it holds: CRC16.check(CRC16.appendCRC(b)) == Some(b)

  def spec: Spec[TestEnvironment with Scope, Any] =
    suite("CRC16 Spec")(
      test("boot up") {
        for {
          result <- ZIO.succeed(CRC16(Chunk(0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef).map(_.toByte)))
        } yield assertTrue(result.toBytes == Chunk(0xe6, 0xf8).map(_.toByte))
      },
      test("example computed with copla") {
        for {
          result <- ZIO.succeed(CRC16(Chunk(0x00, 0x03, 0x00, 0x00, 0x00, 0x0a).map(_.toByte)))
        } yield assertTrue(result.toBytes == Chunk(0xc4, 0x1c).map(_.toByte))
      },
      test("computing and checking CRC are consistent") {
        check {
          for {
            bytes <- Gen.listOfBounded(5, 254)(Gen.byte)
            chunk = Chunk.fromIterable(bytes)
            crc = CRC16(chunk)
            wrongLow <- Gen.byte
            wrongHigh <- Gen.byte
            wrongCRC = Chunk(wrongLow, wrongHigh)
            if wrongCRC != crc.toBytes
          } yield CRC16.check(chunk ++ wrongCRC)
        } { x => assertTrue(x.isEmpty) }
      } @@ TestAspect.samples(100),
      test("gen byte is quite a bit faster than in combination with list above...") {
        check(Gen.byte) { _ =>
          for {
            _ <- ZIO.succeed(())
          } yield assertTrue(true)
        }
      } @@ TestAspect.samples(10000)
    ) @@ TestAspect.sequential @@ TestAspect.timed
}
