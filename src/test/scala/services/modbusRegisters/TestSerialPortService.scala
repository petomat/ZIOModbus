package services.modbusRegisters

import zio.Chunk
import zio.Ref
import zio.ZIO
import zio.ZLayer
import services.serialPort.api.SerialPortService


// hint for testing: socat -d -d pty,raw,crnl,echo=0,link=./VirtualSerialPort1 pty,raw,crnl,echo=0,link=./VirtualSerialPort2

case class TestSerialPortService(writtenBytes: Ref[Chunk[Byte]]) extends SerialPortService {
  val readAllAvailableBytesButAtLeastOneByte: ZIO[Any, String, Chunk[Byte]] = ???
  def writeBytes(bytes: Chunk[Byte]): ZIO[Any, String, Unit] = writtenBytes.update(_ ++ bytes)

  def readBytesAtMost(count: Int): ZIO[Any, String, Chunk[Byte]] = ???
}

object TestSerialPortService {
  def layer: ZLayer[Any, Nothing, TestSerialPortService] = {
    ZLayer {
      for {
        writtenBytes <- Ref.make(Chunk.empty[Byte])
      } yield TestSerialPortService(writtenBytes)
    }
  }
}
